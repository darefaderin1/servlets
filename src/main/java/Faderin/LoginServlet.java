package Faderin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet", value = "/LoginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter printWriter=response.getWriter();
        response.setContentType("text/html");

        String user=request.getParameter("userName");
        String pass=request.getParameter("userPassword");

        if(user.equals("faderin") && pass.equals("complex$$45"))
            printWriter.println("Login Success...!");
        else
            printWriter.println("Login Failed...!");
        printWriter.close();
    }
}
